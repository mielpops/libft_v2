/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tony <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/16 12:37:25 by tony              #+#    #+#             */
/*   Updated: 2019/08/17 15:43:29 by toto             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strlcat(char *dst, const char *src, size_t dstsize)
{
	const char	*s = src;
	char 		*d;
	size_t 		n;
	size_t 		dlen;

	n = dstsize;
	d = dst;
	while (n-- != 0 && *d != '\0')
		d++;
	dlen = d - dst;
	n = dstsize	- dlen;
	if (n == 0)
		return(dlen + ft_strlen(s));
	while (*s != '\0')
	{
		if (n != 1) 
		{
			*d++ = *s;
			n--;
		}
		s++;
	}
	*d = '\0';
	return(dlen + (s - src));
}
